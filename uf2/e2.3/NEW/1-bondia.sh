#!/bin/bash
# Filename:       script.sh
# Author:         Bruguera López, Marc  (ism4929815)
# Date:           03/02/2020
# Version:        0.1
# License:        This is free software, licensed under the GNU General Public License v3.
#                 See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:          ./script.sh

echo "Quin es el teu nom?"
read NOM
echo "Bon dia $NOM"
