#!/bin/bash
# Filename:       script.sh
# Author:         Bruguera López, Marc  (ism4929815)
# Date:           03/02/2020
# Version:        0.1
# License:        This is free software, licensed under the GNU General Public License v3.
#                 See http://www.gnu.org/licenses/gpl.html for more information.
# Usage:          ./script.sh
rm sortida.txt
touch $1
touch $2
echo $RANDOM > $2
echo $RANDOM > $1
touch sortida.txt
echo $1 $2 > $3 
cat $1 $2 > $3
rm $1 $2
