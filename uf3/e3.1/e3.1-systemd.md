# E3. Exercici 1. SystemD

## Introducció

## Continguts

Amb els fiters i directoris de la carpeta exercici responeu les segûents preguntes:

Indiqueu en cada pregunta l'ordre i també el resultat obtingut (podeu copiar del terminal)

## Entrega

1. ** Instal.leu al vostre ordinador el servidor web apache2 (httpd). Assegureu-vos que el teniu enabled**.
   - Ordre per instal.lar-lo:
   - Ordre per posar-lo a 'enabled':
   - Ordre i sortida per veure'n l'estat:
2. ** Creeu ara un servei myweb.service que arrenqui un servidor web python amb el mòdul SimpleHTTPServer. Feu que el fitxer estigui a /usr/lib/systemd/system.**
  - Contingut del fitxer:

3. ** Feu que aquest servidor s'activi en iniciar-se el sistema. **
  - Ordre per fer que s'iniciï amb el sistema:
  - Expliqueu què ha fet aquesta ordre:
  
4. ** Reinicieu el vostre ordinador. Comproveu com es troben els dos serveis, httpd i myweb.**
  - Ordre i sortida de l'estat de httpd:
  - Ordre i sortida de l'estat de myweb:
  - Expliqueu què ha passat.

5. ** Mostreu l'arbre d'arrencada de serveis on es vegi quin dels serveis s'ha iniciat abans.**
  - Ordre i sortida:
  
6. ** Desactiveu el servei httpd de l'arrencada i reinicieu.**
  - Ordre i sortida de l'estat de httpd:
  - Ordre i sortida de l'estat de myweb:
  - Expliqueu què ha passat.

7. ** Feu que el vostre sistema s'iniciï amb el 'target' multi-user i reinicieu per veure què ha passat.**
  - Ordre per establir per defecte el multi-user
  - Expliqueu què ha passat en reiniciar.
  
8. ** Retorneu el vostre sistema al 'target' graphical i reinicieu.**
  - Ordre per establir per defecte el multi-user
  - Expliqueu què ha passat en reiniciar.
